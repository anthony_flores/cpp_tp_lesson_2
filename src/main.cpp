#include <iostream>
#include <vector>

void swap(int * nbFirst, int * nbSecond);
void fct0(std::vector<int> nbs, int length, int * min, int * max);
void fct1(int *array_Pointer, int answer);

int main(int argc, char *argv[]){
    std::cout << "Part 1: Swap numbers" << std::endl;
    int nb_1 = 5;
    int nb_2 = 12;
    swap(&nb_1, &nb_2);
    std::cout << nb_1 << std::endl; //12
    std::cout << nb_2 << std::endl; //5
    std::cout << *&nb_1 << std::endl; //12
    std::cout << *&nb_2 << std::endl; //5


    std::cout << "Part 2: Found min and max in array" << std::endl;
    std::vector<int> number{ 1, 10, 45, 56, 32, 22, 78, 64 };
    int length = 8;
    int min;
    int max;
    fct0(number, length, &min, &max);
    std::cout << "min : " << min << std::endl; //1
    std::cout << "max : " << max << std::endl; //78


    std::cout << "Part 3: Complete a table" << std::endl;
    int tab[5];
    int *array_Pointer = tab;
    fct1(array_Pointer, 5);
    std::cout << "Show array" << std::endl;
    for(int x : tab){
        std::cout << x << std::endl;
    }

    return 0;
}




void swap(int * nbFirst, int * nbSecond){
    *nbFirst = *nbFirst + *nbSecond;
    *nbSecond = *nbFirst - *nbSecond;
    *nbFirst = *nbFirst - *nbSecond;
}

void fct0(std::vector<int> nbs, int length, int * min, int * max){
    for(int i = 0; i < length; i++){
        if (i == 0 || nbs[i] < *min) *min = nbs[i];
		if (i == 0 || nbs[i] > *max) *max = nbs[i];
    }
}

void fct1(int *array_Pointer, int answer){
    for(int i = 0; i < answer; i++){
        int a;
        std::cout << "Choose number to add" << std::endl;
        std::cin >> a;
        array_Pointer[i] = a;
    }
}




